<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Inicio del usuario</title>
</head>
<body>
	<div class="container my-auto">
		<div class="row">
			<div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12 text-center">
				<h1 class="fw-bold text-center py-5">Comparte</h1>
				<p>
					Esta es tu página de comparte, 
					<%=request.getSession().getAttribute("uname") %>
				</p>
				<br>
				<p>
					<a href="upload.jsp">Subir un apunte</a> <br>
				<p>
					<a href="logout.html">Cerrar sesión</a> <br>
			   </p>
			</div>
	   </div>
	</div>
</body>
</html>