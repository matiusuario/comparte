<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Carga de archivos</title>
</head>
<body>
	<div class="container my-auto">
		<div class="row">
			<div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12 text-center">
				<h1 class="fw-bold text-center py-5">Comparte</h1>
				<form method="post" action="Upload" enctype="multipart/form-data">
                <%=request.getSession().getAttribute("uname") %>
                <%=request.getSession().getAttribute("idusuario") %>
                <table>
                	<tr>
                    	<td>Nombre para el apunte: </td>
                    	<td><input type="text" name="fileName" size="50"/></td>
                	</tr>
                	<tr>
                    	<td>Una descripción: </td>
                    	<td><input type="text" name="descrip" size="50"/></td>
                	</tr>
                	<tr>
                    	<td>Archivo: </td>
                    	<td><input type="file" name="file" size="50"/></td>
                	</tr>
                	<tr>
                    	<td colspan="2">
                        <input type="submit" value="Save">
                    	</td>
                	</tr>
            	</table>
        		</form>
				<br>
				<p>
					<a href="logout.html">Cerrar sesión</a> <br>
			   </p>
			</div>
	   </div>
	</div>
</body>
</html>