package servlet;

import entities.*;
import logic.*;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.Servlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;


@MultipartConfig(maxFileSize = 16177215)
/**
 * Servlet implementation class Upload
 */
public class Upload extends HttpServlet implements Servlet {
    
	private static final long serialVersionUID = 1L;
	
    public Upload() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InputStream inputStream = null;
		Archivo a = new Archivo();
		ControlArchivo ctr = new ControlArchivo();
		PrintWriter out = response.getWriter();
		int idus;
		String idust;
		
		HttpSession session = request.getSession(false);
		if (session != null) {
			Part filePart = request.getPart("file");
	        if (filePart != null) {
	            inputStream = filePart.getInputStream();
	            a.setNombre(request.getParameter("fileName"));
		        a.setDescrip(request.getParameter("descrip"));
		        idust = (String)session.getAttribute("idusuario");
		        out.print("ID de usuario: " + idust + "Uname: " + session.getAttribute("uname"));
		        a.setIdUsuario(Integer.parseInt(idust));
		        a.setStream(inputStream);
		        ctr.add(a);
		        out.print("Se subió correctamente");
	        } else {
	        	out.print("No se pudo obtener el InputStream de Part");
	        }
	        
	        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/user.jsp");
			rd.forward(request, response);
		}
	}
}
