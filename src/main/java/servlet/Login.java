package servlet;

import entities.*;
import logic.*;
import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Login() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.getRequestDispatcher("login.html").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		String uname, pwd;
		Usuario u = new Usuario();
		ControlABMUsuario ctr = new ControlABMUsuario();
		
		uname = request.getParameter("username");
		pwd = request.getParameter("password");
		if (uname.length() < 2 || pwd.length() < 2) {
			response.getWriter().append("Complete los campos. ");
		} else {
			u.setNombreUsuario(uname);
			u.setPassword(pwd);
			u = ctr.getByCredentials(u);
			if (u != null) {
				HttpSession session = request.getSession(true);
				session.setAttribute("uname", uname);
				session.setAttribute("idusuario", Integer.toString(u.getId()));
				RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/user.jsp");
				rd.forward(request, response);
			} else {
				response.getWriter().append("Usuario o contrase�a no valido");
			}
		}
	}
}