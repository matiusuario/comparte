package servlet;

import java.io.IOException;

import entities.Usuario;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logic.ControlABMUsuario;

/**
 * Servlet implementation class Signup
 */
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Signup() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.getRequestDispatcher("signup.html").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String uname, pwd, email;
		Usuario u = new Usuario(), userVal;
		ControlABMUsuario ctr = new ControlABMUsuario();
		
		uname = request.getParameter("username");
		pwd = request.getParameter("password");
		email = request.getParameter("email"); 
		if (uname.length() < 2 || pwd.length() < 2) {
			response.getWriter().append("Complete los campos. ");
		} else {
			u.setNombreUsuario(uname);
			u.setPassword(pwd);
			u.setEmail(email);
			u.setTipo("basic"); //A IMPLEMENTAR!!
			userVal = ctr.fetchByNombreUsuario(u);
			if (userVal != null) {
				response.getWriter().append("Este nombre de usuario ya existe. Introduzca otro ");
			} else {
				ctr.add(u);
				response.getWriter().append("�Creado con Exito!");
			}
		}
	}

}
