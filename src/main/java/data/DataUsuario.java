package data;

import entities.*;
import java.sql.*;

public class DataUsuario {
	
	public Usuario getByCredentials(Usuario u){
		Usuario user = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DbConnector.getInstancia().getConn().prepareStatement(
					"SELECT * FROM usuario WHERE nombre_usuario=? AND password=?");
			stmt.setString(1, u.getNombreUsuario());
			stmt.setString(2, u.getPassword());
			rs = stmt.executeQuery();
			if (rs != null && rs.next()) {
				user = new Usuario();
				user.setNombreUsuario(rs.getString("nombre_usuario"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
				user.setTipo("basic");
				user.setId(rs.getInt("idusuario"));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(rs!=null) {rs.close();}
				if(stmt!=null) {stmt.close();}
				DbConnector.getInstancia().releaseConn();
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}
	
	public void add(Usuario u) {
		
		PreparedStatement stmt = null;
		
		try {
			stmt = DbConnector.getInstancia().getConn().prepareStatement(
					"INSERT INTO usuario (nombre_usuario, password, email, tipo) VALUES (?,?,?,?)");
			stmt.setString(1, u.getNombreUsuario());
			stmt.setString(2, u.getPassword());
			stmt.setString(3, u.getEmail());
			stmt.setString(4, u.getTipo());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				if(stmt!=null) {stmt.close();}
				DbConnector.getInstancia().releaseConn();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Usuario fetchByNombreUsuario(Usuario u) {
		
		Usuario user = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DbConnector.getInstancia().getConn().prepareStatement(
					"SELECT nombre_usuario, email FROM usuario WHERE nombre_usuario=?");
			stmt.setString(1, u.getNombreUsuario());
			rs = stmt.executeQuery();
			if (rs != null && rs.next()) {
				user = new Usuario();
				user.setNombreUsuario(rs.getString("nombre_usuario"));
				user.setEmail(rs.getString("email"));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(rs!=null) {rs.close();}
				if(stmt!=null) {stmt.close();}
				DbConnector.getInstancia().releaseConn();
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}
}