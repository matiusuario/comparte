package data;

import entities.*;
import java.sql.*;

public class DataArchivo {
	
	public void add(Archivo a) {
		
		PreparedStatement stmt = null;
		
		try {
			stmt = DbConnector.getInstancia().getConn().prepareStatement(
					"INSERT INTO archivo (nombre_archivo, descrip, idusuario, stream) VALUES (?,?,?,?)");
			stmt.setString(1, a.getNombre());
			stmt.setString(2, a.getDescrip());
			stmt.setInt(3, a.getIdUsuario());
			stmt.setBlob(4, a.getStream());
            int i = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				if(stmt!=null) {stmt.close();}
				DbConnector.getInstancia().releaseConn();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
