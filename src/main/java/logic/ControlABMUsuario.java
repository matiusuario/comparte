package logic;

import entities.*;
import data.*;

public class ControlABMUsuario {
	
	private DataUsuario dataUsuario;
	
	public ControlABMUsuario() {
		dataUsuario = new DataUsuario();
	}
	
	public void add(Usuario u) {
		dataUsuario.add(u);
	}
	
	public Usuario getByCredentials(Usuario u) {
		return dataUsuario.getByCredentials(u);
	}
	
	public Usuario fetchByNombreUsuario(Usuario u) {
		return dataUsuario.fetchByNombreUsuario(u);
	}
}
